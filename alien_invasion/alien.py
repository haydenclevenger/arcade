import pygame
from pygame.sprite import Sprite

class Alien(Sprite):
	"""A class to define a single alien for Alien Invasion"""

	def __init__(self, screen, settings):
		"""Initialize the alien and set its starting position"""
		# Inherit properly from Sprite
		super(Alien, self).__init__()
		self.screen = screen
		self.settings = settings

		#Load the alien image and get its rect
		try:
			self.image = pygame.image.load('images/alien.bmp')
		except:
			self.image = pygame.image.load('alien_invasion/images/alien.bmp')
		self.rect = self.image.get_rect()
		self.screen_rect = screen.get_rect()

		# Start each new alien at the top left
		self.rect.x = self.rect.width
		self.rect.y = self.rect.height

		# Store the alien's exact position
		self.x = float(self.rect.x)

		# Store aliens direction (1 means right, -1 means left)
		self.direction = 1

	def update(self):
		"""Update the alien's horizontal position"""
		if (self.rect.right >= self.screen_rect.right) or (self.rect.left < 0):
			self.rect.y += self.settings.alien_drop
			self.direction *= -1
		# Move alien left or right
		self.x += self.settings.alien_speed * self.direction
		self.rect.x = self.x

	def check_edges(self):
		"""Return True if alien reaches edge of screen"""
		if (self.rect.right >= self.screen_rect.right) or (self.rect.left < 0):
			return True

	def draw_alien(self):
		"""Draw the alien at its current location"""
		self.screen.blit(self.image, self.rect)
