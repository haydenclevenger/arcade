import sys
from time import sleep

import pygame

from bullet import Bullet
from alien import Alien

def check_events(settings, screen, ship, bullets):
	"""Respond to key-presses and mouse clicks"""
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			sys.exit()
		elif event.type == pygame.KEYDOWN:
			check_keydown_events(event, settings, screen, ship, bullets)
		elif event.type == pygame.KEYUP:
			check_keyup_events(ship, event)

def check_keydown_events(event, settings, screen, ship, bullets):
	"""Respond to key-presses"""
	if event.key == pygame.K_RIGHT:
		ship.moving_right = True
	elif event.key == pygame.K_LEFT:
		ship.moving_left = True
	elif event.key == pygame.K_SPACE:
		fire_bullet(settings, screen, ship, bullets)
	elif event.key == pygame.K_q:
		sys.exit()
		

def check_keyup_events(ship, event):
	"""Respond to key-releases"""
	if event.key == pygame.K_RIGHT:
		ship.moving_right = False
	elif event.key == pygame.K_LEFT:
		ship.moving_left = False

def fire_bullet(settings, screen, ship, bullets):
	"""Method to fire a bullet from the ship if limit not yet reached"""
	# Add new bullet to bullets Group
	if len(bullets) < settings.bullet_limit:
		bullets.add(Bullet(settings, screen, ship))


def update_bullets(bullets):
	"""Method to update all bullets position's and remove old bullets"""
	bullets.update()
	for bullet in bullets.copy():
		if bullet.rect.bottom <= 0:
			bullets.remove(bullet)


def create_fleet(settings, screen, aliens, ship):
	"""Method to create a full fleet of aliens"""
	# Find number of aliens in a row
	alien = Alien(screen, settings)
	number_aliens_x = get_number_aliens(settings, alien.rect.width)
	# Find number of rows of aliens
	number_rows = get_number_rows(settings, ship.rect.height, alien.rect.height)

	# Create fleet of aliens
	for x in range(number_aliens_x):
		for y in range(number_rows):
			create_alien(settings, screen, aliens, x, y)

def get_number_aliens(settings, alien_width):
	"""Method to calculate the number of aliens that will fit across the screen"""
	available_space_x = settings.screen_width
	return int(available_space_x / (alien_width * 2))

def get_number_rows(settings, ship_height, alien_height):
	"""Method to calculate the number of rows of aliens"""
	available_space_y = settings.screen_height
	return int((available_space_y - (3 * alien_height + ship_height)) / (2 * alien_height))

def create_alien(settings, screen, aliens, alien_number, row_number):
	"""Create an alien and place it in its place"""
	alien = Alien(screen, settings)
	alien.x = alien_number * 2 * alien.rect.width
	alien.rect.x = alien.x
	alien.y = row_number * 2 * alien.rect.height
	alien.rect.y = alien.y
	aliens.add(alien)

def update_aliens(settings, stats, aliens, bullets, screen, ship):
	"""Update alien positions and check for new level"""
	aliens.update()
	if len(aliens) == 0:
		next_level(settings, stats, aliens, bullets, screen, ship)

def check_collisions(bullets, aliens, ship, stats, settings, screen):
	"""Method to check for collisions between bullets, aliens, and the ship"""
	# Check for any bullets that have hit aliens
	collisions = pygame.sprite.groupcollide(bullets, aliens, True, True)
	# Check for any aliens that have hit the ship
	if pygame.sprite.spritecollideany(ship, aliens):
		stats.ships_left -= 1
		print("Ship Hit! - Ships Remaining: " + str(stats.ships_left))
		# Check for Game Over
		if stats.ships_left <= 0:
			print("Game Over")
			sleep(1)
			exit()
		# Reset level
		else:
			bullets.empty()
			aliens.empty()
			sleep(1)
			create_fleet(settings, screen, aliens, ship)


def next_level(settings, stats, aliens, bullets, screen, ship):
	"""Method to pause and then begin next level"""
	sleep(1)
	aliens.empty()
	bullets.empty()
	settings.alien_speed *= 2
	settings.alien_drop += 10
	stats.level += 1
	create_fleet(settings, screen, aliens, ship)
	print("Level " + str(stats.level) + "!")

def update_screen(settings, screen, ship, bullets, aliens):
	"""Redraw all surfaces"""
	# Redraw the surfaces during each pass through the loop
	screen.fill(settings.bg_color)
	ship.draw_ship()
	# Redraw all bullets
	for bullet in bullets.sprites():
		bullet.draw_bullet()

	# Redraw all aliens
	for alien in aliens.sprites():
		alien.draw_alien()

	#Make the most recently drawn screen visible
	pygame.display.flip()
