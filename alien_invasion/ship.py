import pygame

class Ship():
	"""A class to define our ship for Alien Invasion"""

	def __init__(self, screen, settings):
		"""Initialize the ship and set its starting position"""
		self.screen = screen
		self.settings = settings
		#Flags to allow continuous movement
		self.moving_right = False
		self.moving_left = False

		#Load the ship image and get its rect
		try:
			self.image = pygame.image.load('images/ship.bmp')
		except:
			self.image = pygame.image.load('alien_invasion/images/ship.bmp')
		self.rect = self.image.get_rect()
		self.screen_rect = screen.get_rect()

		# Start each new ship at the bottom center of the screen
		self.rect.centerx = self.screen_rect.centerx
		self.rect.bottom = self.screen_rect.bottom

	def update(self):
		"""Update the ship's position based on the movement flags"""
		if self.moving_right and self.rect.right < self.settings.screen_width:
			self.rect.centerx += self.settings.ship_speed
		if self.moving_left and self.rect.left > 0:
			self.rect.centerx -= self.settings.ship_speed

	def draw_ship(self):
		"""Draw the ship at its current location"""
		self.screen.blit(self.image, self.rect)
