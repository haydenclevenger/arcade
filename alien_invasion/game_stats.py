class GameStats():
	"""Track statistics for Alien Invasion"""

	def __init__(self, settings):
		"""Initialize statistics"""
		self.settings = settings
		self.reset_stats()

	def reset_stats(self):
		"""Initialize statistics that can change during the game"""
		self.ship_limit = self.settings.ship_limit
		self.ships_left = self.ship_limit
		self.level = 0
