# This code is for the Alien Invasion game
# Author: Hayden Clevenger - hayden.clev@gmail.com
# Date: 14 April, 2018

import pygame
from pygame.sprite import Group

from settings import Settings
from ship import Ship
from bullet import Bullet
from game_stats import GameStats
import game_functions as gf

def run_game():
	"""Method for running the Alien Invasion game"""

	# Setup pygame backround settings
	pygame.init()

	# Setup custom game settings
	settings = Settings()

	# Create screen
	screen = pygame.display.set_mode((settings.screen_width, settings.screen_height))
	pygame.display.set_caption("Alien Invasion")

	# Create ship
	ship = Ship(screen, settings)

	# Create a group for bullets
	bullets = Group()

	# Create aliens
	aliens = Group()
	gf.create_fleet(settings, screen, aliens, ship)

	# Create game stats
	stats = GameStats(settings)

	# Start the main loop for the game
	while True:
		# Watch for keyboard and mouse events
		gf.check_events(settings, screen, ship, bullets)
		ship.update()
		gf.update_bullets(bullets)
		gf.update_aliens(settings, stats, aliens, bullets, screen, ship)
		gf.check_collisions(bullets, aliens, ship, stats, settings, screen)

		# Redraw all surfaces
		gf.update_screen(settings, screen, ship, bullets, aliens)



run_game()
