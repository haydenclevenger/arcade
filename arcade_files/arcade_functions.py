import pygame
import pygame.freetype as freetype
from subprocess import run

def create_list(screen, settings):
	"""Method to make a formatted list of available games"""
	games = []
	font = freetype.SysFont('Arial', 10, False, False)
	for game in settings.games:
		text = font.render(game, size=10)
		text[1].centerx = screen.get_rect().centerx
		# Center first entry
		if not games:
			text[1].centery = screen.get_rect().centery
		# Put everything else below
		else:
			text[1].top = games[-1][1].bottom + 3
		games.append(text)

	return games

def events(cursor, settings, games):
	"""Method to watch for keyboard events"""
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			exit()
		# Keydown events
		elif event.type == pygame.KEYDOWN:
			if event.key == pygame.K_RETURN:
				launch_game(cursor, settings)
			elif event.key == pygame.K_DOWN:
				# Move cursor down
				cursor.update_cursor(settings, games, down=True)
			elif event.key == pygame.K_UP:
				# Move cursor up
				cursor.update_cursor(settings, games, down=False)
			elif event.key == pygame.K_q:
				exit()

def launch_game(cursor, settings):
	"""Method to launch the selected game"""
	command = settings.games[cursor.position]
	command = command.strip().lower().replace(" ", "_")
	try:
		run(["python3",  command + "/" + command + ".py"])
	except:
		print("Exception")

def update_screen(screen, game_list, cursor, settings):
	"""Method to redraw all surfaces"""
	# Draw screen
	screen.fill(settings.bg_color)
	# Draw game list
	for game in game_list:
		screen.blit(game[0], game[1])
	# Draw cursor
	cursor.draw_cursor(screen)

	pygame.display.flip()
