# A class for a cursor

import pygame

class Cursor:

	def __init__(self, item):
		"""Initialize cursor and point it at item"""
		self.screen = pygame.display.get_surface()
		self.image = pygame.image.load('arcade_files/cursor.bmp')
		self.rect = self.image.get_rect()
		self.rect.right = item.left
		self.rect.y = item.y
		self.position = 0

	def update_cursor(self, settings, games, down):
		"""Method to update the cursors position"""
		# Move cursor down
		if down:
			if not self.position >= len(settings.games)-1:
				self.position += 1
		# Move cursor up
		elif not down:
			if not self.position <= 0:
				self.position -= 1
		self.rect.right = games[self.position][1].left
		self.rect.y = games[self.position][1].y
		print("Position: " + str(self.position))

	def draw_cursor(self, screen):
		"""Method to draw the cursor"""
		self.screen.blit(self.image, self.rect)
