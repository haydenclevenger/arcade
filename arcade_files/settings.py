class Settings():
	"""Class to hold settings for the arcade"""

	def __init__(self):
		"""Initialize settings"""
		# Screen settings
		self.screen_height = 250
		self.screen_width = 400
		self.bg_color = (150, 150, 150)

		# Game list settings
		self.games = ['Alien Invasion', 'Snake', 'Life']
		self.font_color = (0, 0, 0)
