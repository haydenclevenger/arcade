# This code launches a virtual arcade
# It allows the user to select which game they would like to play

#Author: Hayden Clevenger - hayden.clev@gmail.com
#Date 4 February, 2019

import pygame
import arcade_files.arcade_functions as af

from arcade_files.settings import Settings
from arcade_files.cursor import Cursor

def run_game():
	"""Method for running the Arcade"""

	# Setup background settings
	pygame.init()

	# Initialize settings
	settings = Settings()

	# Create screen
	screen = pygame.display.set_mode((settings.screen_width, settings.screen_height))
	pygame.display.set_caption("Arcade")

	# Create game list
	games = af.create_list(screen, settings)

	# Create selector
	cursor = Cursor(games[0][1])

	while True:
		# Watch for keyboard and mouse events
		af.events(cursor, settings, games)
		# Redraw surfaces
		af.update_screen(screen, games, cursor, settings)

run_game()
