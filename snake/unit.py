import pygame
from pygame.sprite import Sprite

class Unit(Sprite):
	"""A class to manage each unit in a Snek"""

	def __init__(self, settings, screen, direction='n'):
		"""
			Initialize a unit
			Direction indicates movement (n=up, s=down, e=right, w=left)
		"""
		super().__init__()
		
		self.screen = screen
		self.color = settings.snake_color
		self.speed = settings.snake_speed
		self.direction = direction
		# Create rect at 0,0
		self.rect = pygame.Rect(0, 0, settings.snake_unit_size, settings.snake_unit_size)

	def draw_unit(self):
		"""Method to draw unit to screen"""
		pygame.draw.rect(self.screen, self.color, self.rect)
