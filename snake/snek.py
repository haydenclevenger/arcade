import pygame
from pygame.sprite import Sprite

from unit import Unit

class Snek():
	"""This class describes a Snek for player control when playing Snake"""

	def __init__(self, settings, screen):
		"""Create new Snek"""
		self.screen = screen
		self.color = settings.snake_color
		self.speed = settings.snake_speed

		# Create head Sprite of snek
		head = Unit(settings, screen)
		head.rect.centerx = screen.get_rect().centerx
		head.rect.centery = screen.get_rect().centery
		self.body = [head]

	def add_unit(self, settings):
		"""Method to add a unit to a snek"""
		butt = Unit(settings, self.screen, self.body[-1].direction)
		
		# Place rect based on direction of last Snek Unit
		if self.body[-1].direction == 'n':
			butt.rect.centerx = self.body[-1].rect.centerx
			butt.rect.top = self.body[-1].rect.bottom
		elif self.body[-1].direction == 's':
			butt.rect.centerx = self.body[-1].rect.centerx
			butt.rect.bottom = self.body[-1].rect.top
		elif self.body[-1].direction == 'e':
			butt.rect.centery = self.body[-1].rect.centery
			butt.rect.right = self.body[-1].rect.left
		elif self.body[-1].direction == 'w':
			butt.rect.centery = self.body[-1].rect.centery
			butt.rect.left = self.body[-1].rect.right

		self.body.append(butt)
		print("Length: " + str(len(self.body)))

	def update(self, settings):
		"""Method to update all snake units to create illusion of wraparound motion"""
		d = self.body[0].direction
		for unit in self.body:
			if unit.direction == 'n':
				unit.rect.y -= self.speed
				if unit.rect.y < 0:
					unit.rect.y = settings.screen_height + unit.rect.y
			elif unit.direction == 's':
				unit.rect.y += self.speed
				if unit.rect.y > settings.screen_height:
					unit.rect.y -= settings.screen_height
			elif unit.direction == 'e':
				unit.rect.x += self.speed
				if unit.rect.x  > settings.screen_width:
					unit.rect.x -= settings.screen_width
			elif unit.direction == 'w':
				unit.rect.x -= self.speed
				if unit.rect.x < 0:
					unit.rect.x = settings.screen_height + unit.rect.x
			temp = unit.direction
			unit.direction = d
			d = temp


	def draw(self):
		"""Method to draw the Snek"""
		for unit in self.body:
			pygame.draw.rect(self.screen, self.color, unit.rect)
