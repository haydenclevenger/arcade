# This code implements the game Snake
# Author: Hayden Clevenger - hayden.clev@gmail.com
# Date: 5 February, 2019

import pygame
from time import sleep

from settings import Settings
from snek import Snek
import game_functions as gf

def run_game():
	"""Method for running the game Snake"""
	# Setup pygame
	pygame.init()

	# Setup custom game settings
	settings = Settings()

	# Create screen
	screen = pygame.display.set_mode((settings.screen_width, settings.screen_height))
	pygame.display.set_caption("Snake")

	# Create Snek
	snek = Snek(settings, screen)

	# Add an initial food sprite
	food = pygame.Rect(0, 0, settings.snake_unit_size, settings.snake_unit_size)
	gf.relocate_food(settings, food)

	while True:
		sleep(0.1)	# Sleep to make game speed reasonable
		gf.check_events(snek)
		snek.update(settings)
		gf.check_collisions(settings, food, snek)
		gf.update_screen(settings, screen, snek, food)

run_game()
