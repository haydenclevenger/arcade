class Settings():
	"""This class describes the settings for the game Snake"""

	def __init__(self):
		"""Create new settings"""

		# Screen settings
		self.screen_width = 500
		self.screen_height = 500
		self.bg_color = (200, 200, 200)

		# Snake settings
		self.snake_color = (0, 0, 0)
		self.snake_unit_size = 10
		self.snake_speed = self.snake_unit_size * 1
