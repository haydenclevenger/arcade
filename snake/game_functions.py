import pygame

from random import randint
from time import sleep

def check_events(snek):
	"""Method to handle game events"""
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			exit()
		elif event.type == pygame.KEYDOWN:
			keydown_event(event, snek)

def keydown_event(event, snek):
	"""Method to handle keydown events"""
	if event.key == pygame.K_q:
		exit()
	elif event.key == pygame.K_UP:
		if not snek.body[0].direction == 's':
			snek.body[0].direction = 'n'
	elif event.key == pygame.K_DOWN:
		if not snek.body[0].direction == 'n':
			snek.body[0].direction = 's'
	elif event.key == pygame.K_RIGHT:
		if not snek.body[0].direction == 'w':
			snek.body[0].direction = 'e'
	elif event.key == pygame.K_LEFT:
		if not snek.body[0].direction == 'e':
			snek.body[0].direction = 'w'

def relocate_food(settings, food):
	"""Method to make food appear somewhere on the screen"""
	food.x = randint(0, settings.screen_width)
	food.y = randint(0, settings.screen_height)

def check_collisions(settings, food, snek):
	"""Method to check for collisons between food, Snek and itself"""
	# Food collisions
	if food.colliderect(snek.body[0].rect):
		relocate_food(settings, food)
		snek.add_unit(settings)

	# Snek collisions
	for unit in snek.body[1:]:
		if snek.body[0].rect.colliderect(unit.rect):
			sleep(3)
			exit()

def update_screen(settings, screen, snek, food):
	"""Method to redraw the new game screen"""
	screen.fill(settings.bg_color)
	snek.draw()
	pygame.draw.rect(screen, (0,0,0), food)
	pygame.display.flip()
