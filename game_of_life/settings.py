class Settings():
	'''This class holds settings for Conway's Game of Life'''

	def __init__(self, color_mode=False):
		'''Initialize settings object'''
		# Screen settings
		self.screen_width = 800
		self.screen_height = 500
		self.bg_color = (0, 0, 0)

		# Functionality
		self.paused = False	# Pause feature
		self.speed = 0.0	# Time delay between generations

		# Cell settings
		self.cell_color = (200, 200, 200)
		self.color_mode = color_mode
		self.color_change = 10
		self.cell_size = 5
		self.x_cells = int(self.screen_width / self.cell_size)
		self.y_cells = int(self.screen_height / self.cell_size)
		self.num_live = int(self.x_cells * self.y_cells / 5)	# Good live/dead ratio
