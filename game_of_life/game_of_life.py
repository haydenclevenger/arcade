# This code implements Conway's Game of Life
# Author: Hayden Clevenger - hayden.clev@gmail.com
# Date: 3 March, 2019

import sys
import pygame
import argparse
from time import sleep, perf_counter

from settings import Settings
from cells import Cells
import functions as f

def run_game(generations, color_mode, manual, filename):
	"""Method to run the game"""

	# Game settings
	settings = Settings(color_mode)

	# Setup pygame
	pygame.init()


	# Create screen
	screen = pygame.display.set_mode((settings.screen_width, settings.screen_height), pygame.FULLSCREEN)
	screen.set_alpha(None)

	pygame.display.set_caption("Game of Life")

	# Create and activate cells
	cells = Cells(screen, settings)
	cells.activate(settings, manual, filename)

	# Loop for game
	gen = 0
	while gen != generations:
		f.display(screen, settings, cells, gen)
		f.check_events(settings)
		if not settings.paused:
			f.update(settings, cells)
			gen += 1
		sleep(settings.speed) 	# Sleep to control game speed
	
def timing(generations, color_mode, manual, filename):
	'''Method to time game for code optimization'''
	start = perf_counter()
	run_game(generations, color_mode, manual, filename)
	diff = perf_counter() - start
	print(diff)

def wrapper():
	'''Wrapper method to parse the command line args and run appropriate code'''
	parser = argparse.ArgumentParser(description="parse command line args")
	parser.add_argument('-f', metavar='filename', help='file to read in initial pattern from')
	parser.add_argument('-m', '-manual', metavar='manual', action='store_const', 
			const=True, help='select initial live cells manually')
	parser.add_argument('-t', metavar='generations', type=int, default=-1, 
			dest='g', help='time execution of program over specified number of generations')
	parser.add_argument('-c', '-color-mode', metavar='color mode', action='store_const', 
			const=True, help='color changing mode')

	args = parser.parse_args()

	if args.g > 0:
		timing(generations=args.g, color_mode=args.c, manual=args.m, filename=args.f)
	else:
		run_game(generations=args.g, color_mode=args.c, manual=args.m, filename=args.f)

wrapper()
