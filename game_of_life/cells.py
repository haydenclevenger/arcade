import sys
import pygame
import functions as f
from random import randint

class Cells():
	'''Matrix to hold all cells for Game of Life'''

	def __init__(self, screen, settings):
		'''
			Object to hold matrix of cells and associated information
			1 if active, 0 if not
		'''
		self.screen = screen
		self.color = settings.cell_color
		self.color_change = settings.color_change
		self.size = settings.cell_size

		# Matrix of deactivated cells
		self.matrix = [[0 for i in range(0, settings.y_cells)] for j in range(0, settings.x_cells)]

	def activate(self, settings, manual, filename):
		'''Method to decide which way to activate the cells'''
		if filename:
			self.activate_from_file(settings, filename)
		elif manual:
			self.activate_manual(settings)
		else:
			self.activate_random(limit=settings.num_live)

	def activate_random(self, limit=100):
		'''Activate limit number of cells'''
		for i in range(0, limit):
			while True:		# In case cell activated twice
				x = randint(0, len(self.matrix)-1)	# num columns
				y = randint(0, len(self.matrix[0])-1)		# num rows

				if self.matrix[x][y] == 0:
					self.matrix[x][y] = 1
					break

	def activate_from_file(self, settings, filename):
		'''
			Activate specific cells based on file
			Reads pattern and translates it to middle of screen
			Format: pair of comma seperated coordinates on each line (ex: 4,35)
		'''
		x_mid = int(settings.x_cells / 2)
		y_mid = int(settings.y_cells / 2)
		try:
			with open(filename) as f:
				for line in f.read().splitlines():
					split = line.find(',')
					x = int(line[:split])
					y = int(line[split + 1:])
					self.matrix[x + x_mid][y + y_mid] = 1
		except Exception as e:
			print(e)
			sys.exit()

	def activate_manual(self, settings):
		'''Allow user to click on screen to activate/deactivate cells'''
		# Loop allows user time to choose active cells
		while True:
			for event in pygame.event.get():
				# Mouse events
				if event.type == pygame.MOUSEBUTTONDOWN:
					pos = pygame.mouse.get_pos()
					pos_x = int(pos[0]/self.size)
					pos_y = int(pos[1]/self.size)
					self.matrix[pos_x][pos_y] = 1 if self.matrix[pos_x][pos_y] == 0 else 0	# Flip cell state
					f.display(self.screen, settings, self, 0)	# Display clicked cells

				elif event.type == pygame.KEYDOWN:
					if event.key == pygame.K_RETURN:	# Run game
						return
					elif event.key == pygame.K_q:
						sys.exit()

				elif event.type == pygame.QUIT:
					sys.exit()

	def draw(self):
		'''Method to draw the cells to the screen'''
		for i in range(0, len(self.matrix)):
			for j in range(0, len(self.matrix[0])):
				if self.matrix[i][j] == 1:
					cell = pygame.Rect(
							i * self.size,
							j * self.size,
							self.size, 
							self.size)
					pygame.draw.rect(self.screen, self.color, cell)
