import sys
import pygame

import pygame.freetype as ft
from random import choice


def check_events(settings):
	'''Method to check for game events and exit'''
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			exit()
		elif event.type == pygame.KEYDOWN:
			if event.key == pygame.K_q:
				exit()
			elif event.key == pygame.K_SPACE:	# Pause feature
				settings.paused = False if settings.paused else True

def update(settings, cells):
	'''Method to update all cells'''
	# Change color if running with -c flag
	if settings.color_mode == True:
		update_color(cells)
	# Create copy of array to work with
	copy = [row[:] for row in cells.matrix]
	for i in range(0, len(copy)):
		for j in range(0, len(copy[0])):
			# Check number of live neighbors
			neighbors = count_neighbors(copy, i, j)
			# Update pixel status
			if neighbors < 2 or neighbors > 3:
				cells.matrix[i][j] = 0	# Dead
			elif neighbors == 3:
				cells.matrix[i][j] = 1	# Alive

def update_color(cells):
	'''Method to udate the color of the cells'''
	new_color = []
	for val in cells.color:
		if val < cells.color_change:
			val += choice([0, cells.color_change])
		elif val > 255 - cells.color_change:
			val += choice([-cells.color_change, 0])
		else:
			val += choice([-cells.color_change, 0, cells.color_change])
		new_color.append(val)
	cells.color = tuple(new_color)

def count_neighbors(matrix, i, j):
	'''Count live neighbors of pixel[i,j] in matrix'''
	neighbors = 0
	for x in range(i-1, i+2):
		for y in range(j-1, j+2):
			if(x == i and y == j):
				continue	# Skip self
			else:
				try:
					neighbors += matrix[x][y]
				except IndexError as e:
					neighbors += matrix[x % len(matrix)][y % len(matrix[0])]

	return neighbors

def display_gen(screen, gen):
	'''Method to display the generation count on the screen'''
	font = ft.SysFont('Arial', 20, False, False)
	text = font.render('Generation: ' + str(gen), bgcolor=(200,200,200))
	text[1].left = screen.get_rect().left
	text[1].bottom = screen.get_rect().bottom
	screen.blit(text[0], text[1])

def display(screen, settings, cells, gen):
	'''Method to display cells on the screen'''
	screen.fill(settings.bg_color)
	cells.draw()
	display_gen(screen, gen)
	pygame.display.flip()
